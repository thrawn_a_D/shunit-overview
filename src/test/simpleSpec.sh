#!/bin/bash

testShUnitMethods()
{
     assertEquals "1" "1"
     assertNull '' # Asserts that value is null, or in shell terms a zero-length string. The message is optional.
     assertNotNull 'blub'
     assertSame "1" "1" "1" "1" "1" "1" # Same as assertEquals but with an array of args
     assertNotSame "1" "3"

     # Boolean
     assertTrue true
     assertTrue 0
     assertFalse false
     assertFalse 1

#      fail "IFailed"
#      fail "IFailed 2"
}

setUp() {
    echo "Setting up a single test."
}

tearDown(){
     echo "Cleaning up!"
}

oneTimeSetUp() {
     echo "Setting up the whole test file"
}

oneTimeTearDown() {
     echo "Tearing down the whole test file"
}

testWorkingNonsence()
{
    assertEquals "it works" "it works"
}

# testBrokenNonsence()
# {
#     assertEquals "it works" "it does not work"
# }

source libs/shunit2-2.0.3/src/shell/shunit2
