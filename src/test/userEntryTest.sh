#!/bin/bash

source src/main/userEntry.sh

setUp() {
    echo "Unsetting vars"
    unset ARGUMENT
    unset OPERATION_NAME
}

testReadUserInput_positive()
{
     # GIVEN
     local operationName="ADD"
     local args=(1 2)

     # WHEN
     readUserInput -o $operationName -a $args

     #THEN
     assertSame "The calculation operation is not as expected" ${operationName} $OPERATION_NAME
     assertEquals "The provided parameters are not as expected" "${args}" "$ARGUMENT"
}

testReadUserInput_noInput()
{
     # WHEN
     readUserInput

     #THEN
     assertFalse "There were no parameter provided, but readUserInput did not complain" $?
}

source libs/shunit2-2.0.3/src/shell/shunit2
