#!/bin/bash

source src/main/fibonacci.sh

testFibonacciSeq() {
     # GIVEN
     local fibbo_nbumber=10

     # WHEN
     local result=$(fibonacci $fibbo_nbumber)

     # THEN
     assertEquals 55 $result
}

source libs/shunit2-2.0.3/src/shell/shunit2
