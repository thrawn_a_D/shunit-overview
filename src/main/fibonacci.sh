#!/bin/bash

fibonacci()
{
     local num=$1
     local f1=0
     local f2=1

     for (( i=1;i<$num;i++ ))
     do
          fn=$((f1+f2))
          f1=$f2
          f2=$fn
     done
     echo $f2
}
