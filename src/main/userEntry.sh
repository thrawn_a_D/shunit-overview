#!/bin/bash

usage()
{
     echo "-a [ARGUMENT] -o [OPERATION_NAME]"
}

readUserInput()
{
    if [ $# = 0 ]; then
      echo "Please provide mandatory params:"
      usage
      return 1
    fi
    while getopts a:o: PARAM ; do
      case $PARAM in
         a) ARGUMENT=$OPTARG;;
         o) OPERATION_NAME=$OPTARG;;
      esac
    done
    return 0
}
