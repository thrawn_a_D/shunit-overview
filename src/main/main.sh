#!/bin/bash

source src/main/fibonacci.sh
source src/main/userEntry.sh

readUserInput $@

if [ ! -z $OPERATION_NAME ] && [ "${OPERATION_NAME}" == "FIB" ]; then
     result=$(fibonacci ${ARGUMENT})
     echo "RESULT $result"
fi
