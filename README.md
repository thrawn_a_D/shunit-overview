## Warum Tests

* Automatisierte Qualitätssicherung
* Fehler im Falle von Änderungen fallen schnell auf
* Veranschaulichen wie der Code zu benutzen ist
* Hilfsmittel für sauberen Code

Ausführliche Beschreibung:
http://bytesalad.blogspot.de/2013/12/test-driven-shell-scripting.html


## Unit-Tests im Bereich Shell

### Test-Bibliothek shunit2
* http://code.google.com/p/shunit2/
* Dokumentation:  http://ssb.stsci.edu/testing/shunit2/shunit2.html
* Portabel(sh, bash, dash, ksh, pdksh, zsh)
* JUnit-Like
* Freie Formulierung von Testfällen
* Schnelle Ausführung
* Praktisch als commit-hook integrierbar (.git/hooks/pre-commit)

### Test-Bibliothek bats
* https://github.com/sstephenson/bats