#!/bin/bash

# Setup directories and download libs
mkdir -p {libs,src/{main,test}}
wget -qO- http://downloads.sourceforge.net/shunit2/shunit2-2.0.3.tgz | tar xv -C libs/
